package fr.univparis8.iut.proservice.repository;

import fr.univparis8.iut.proservice.domain.Advertisement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Advertisement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvertisementsRepository extends JpaRepository<Advertisement, Long> {

    List<Advertisement> findAdvertisementByOwner(String userId);
}
