package fr.univparis8.iut.proservice.repository;

import fr.univparis8.iut.proservice.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Post entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PostsRepository extends JpaRepository<Post, Long> {

    List<Post> findAllByOwnerEquals(String login);
}
