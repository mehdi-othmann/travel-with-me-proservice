package fr.univparis8.iut.proservice.service.mapper;

import fr.univparis8.iut.proservice.domain.Post;
import fr.univparis8.iut.proservice.service.dto.PostDTO;
import fr.univparis8.iut.proservice.service.dto.PostsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Post and its DTO PostDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PostsMapper extends EntityMapper<PostsDTO, Post> {


    @Mapping(target = "comments", ignore = true)
    Post toEntity(PostDTO postDTO);

    default Post fromId(Long id) {
        if (id == null) {
            return null;
        }
        Post post = new Post();
        post.setId(id);
        return post;
    }
}
