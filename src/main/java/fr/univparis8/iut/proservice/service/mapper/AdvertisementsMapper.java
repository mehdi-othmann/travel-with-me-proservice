package fr.univparis8.iut.proservice.service.mapper;

import fr.univparis8.iut.proservice.domain.Advertisement;
import fr.univparis8.iut.proservice.service.dto.AdvertisementDTO;
import fr.univparis8.iut.proservice.service.dto.AdvertisementsDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity Advertisement and its DTO AdvertisementDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdvertisementsMapper extends EntityMapper<AdvertisementsDTO, Advertisement> {



    default Advertisement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Advertisement advertisement = new Advertisement();
        advertisement.setId(id);
        return advertisement;
    }
}
