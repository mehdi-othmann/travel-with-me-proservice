package fr.univparis8.iut.proservice.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the Advertisement entity.
 */
public class AdvertisementsDTO implements Serializable {

    private Long id;

    @NotNull
    private String advertisementTitle;

    @NotNull
    private String advertisementImage;

    private String owner;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private LocalDate expirationDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdvertisementTitle() {
        return advertisementTitle;
    }

    public void setAdvertisementTitle(String advertisementTitle) {
        this.advertisementTitle = advertisementTitle;
    }

    public String getAdvertisementImage() {
        return advertisementImage;
    }

    public void setAdvertisementImage(String advertisementImage) {
        this.advertisementImage = advertisementImage;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdvertisementsDTO advertisementDTO = (AdvertisementsDTO) o;
        if (advertisementDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), advertisementDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AdvertisementDTO{" +
            "id=" + getId() +
            ", advertisementTitle='" + getAdvertisementTitle() + "'" +
            ", advertisementImage='" + getAdvertisementImage() + "'" +
            ", owner='" + getOwner() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", expirationDate='" + getExpirationDate() + "'" +
            "}";
    }
}
