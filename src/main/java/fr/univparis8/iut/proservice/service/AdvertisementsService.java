package fr.univparis8.iut.proservice.service;

import fr.univparis8.iut.proservice.domain.Advertisement;
import fr.univparis8.iut.proservice.repository.AdvertisementRepository;
import fr.univparis8.iut.proservice.repository.AdvertisementsRepository;
import fr.univparis8.iut.proservice.service.dto.AdvertisementDTO;
import fr.univparis8.iut.proservice.service.dto.AdvertisementsDTO;
import fr.univparis8.iut.proservice.service.mapper.AdvertisementMapper;
import fr.univparis8.iut.proservice.service.mapper.AdvertisementsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Advertisement.
 */
@Service
@Transactional
public class AdvertisementsService {

    private final Logger log = LoggerFactory.getLogger(AdvertisementsService.class);

    private final AdvertisementsRepository advertisementsRepository;

    private final AdvertisementsMapper advertisementsMapper;

    public AdvertisementsService(AdvertisementsRepository advertisementRepository, AdvertisementsMapper advertisementMapper) {
        this.advertisementsRepository = advertisementRepository;
        this.advertisementsMapper = advertisementMapper;
    }


    public AdvertisementsDTO save(AdvertisementsDTO advertisementDTO) {
        log.debug("Request to save Advertisement : {}", advertisementDTO);
        Advertisement advertisement = advertisementsMapper.toEntity(advertisementDTO);
        advertisement = advertisementsRepository.save(advertisement);
        return advertisementsMapper.toDto(advertisement);
    }


    @Transactional(readOnly = true)
    public List<AdvertisementsDTO> findAll() {
        log.debug("Request to get all Advertisements");
        return advertisementsRepository.findAll().stream()
            .map(advertisementsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Transactional(readOnly = true)
    public List<AdvertisementsDTO> findAllByOwner(String userLogin) {
        return advertisementsRepository.findAdvertisementByOwner(userLogin).stream()
            .map(advertisementsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Transactional(readOnly = true)
    public Optional<AdvertisementsDTO> findOne(Long id) {
        log.debug("Request to get Advertisement : {}", id);
        return advertisementsRepository.findById(id)
            .map(advertisementsMapper::toDto);
    }


    public void delete(Long id) {
        log.debug("Request to delete Advertisement : {}", id);
        advertisementsRepository.deleteById(id);
    }
}
