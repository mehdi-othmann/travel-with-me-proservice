package fr.univparis8.iut.proservice.service;

import fr.univparis8.iut.proservice.repository.PostsRepository;
import fr.univparis8.iut.proservice.service.dto.PostsDTO;
import fr.univparis8.iut.proservice.service.mapper.PostsMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Post.
 */
@Service
@Transactional
public class PostsService {

    private final PostsRepository postsRepository;

    private final PostsMapper postsMapper;

    public PostsService(PostsRepository postsRepository, PostsMapper postsMapper) {
        this.postsRepository = postsRepository;
        this.postsMapper = postsMapper;
    }

    @Transactional(readOnly = true)
    public List<PostsDTO> findUserPosts(String login) {
        return postsRepository.findAllByOwnerEquals(login).stream()
            .map(postsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

}
