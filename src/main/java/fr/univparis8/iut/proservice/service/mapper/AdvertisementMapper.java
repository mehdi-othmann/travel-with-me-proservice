package fr.univparis8.iut.proservice.service.mapper;

import fr.univparis8.iut.proservice.domain.*;
import fr.univparis8.iut.proservice.service.dto.AdvertisementDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Advertisement and its DTO AdvertisementDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdvertisementMapper extends EntityMapper<AdvertisementDTO, Advertisement> {



    default Advertisement fromId(Long id) {
        if (id == null) {
            return null;
        }
        Advertisement advertisement = new Advertisement();
        advertisement.setId(id);
        return advertisement;
    }
}
