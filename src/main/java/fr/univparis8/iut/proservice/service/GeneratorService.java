package fr.univparis8.iut.proservice.service;

import com.github.javafaker.Faker;
import fr.univparis8.iut.proservice.domain.Advertisement;
import fr.univparis8.iut.proservice.domain.Comment;
import fr.univparis8.iut.proservice.domain.Post;
import fr.univparis8.iut.proservice.repository.AdvertisementsRepository;
import fr.univparis8.iut.proservice.repository.CommentRepository;
import fr.univparis8.iut.proservice.repository.PostsRepository;
import fr.univparis8.iut.proservice.web.rest.TripServiceClient;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;

@Service
public class GeneratorService {

    private final PostsRepository postsRepository;

    private final CommentRepository commentRepository;

    private final TripServiceClient tripServiceClient;

    private final AdvertisementsRepository advertisementsRepository;

    public GeneratorService(PostsRepository postsRepository, CommentRepository commentRepository, TripServiceClient tripServiceClient, AdvertisementsRepository advertisementsRepository) {
        this.postsRepository = postsRepository;
        this.commentRepository = commentRepository;
        this.tripServiceClient = tripServiceClient;
        this.advertisementsRepository = advertisementsRepository;
    }

    public void generatePost(int amount) {
        Faker faker = new Faker();
        Random r = new Random();

        List<String> proUsersId = null;
        try {
            proUsersId = tripServiceClient.getAllProUser();
        } catch (Exception e) {
        }

        for (int i = 0; i < amount; i++) {
            int randomPro = r.nextInt(proUsersId.size());

            Post post = new Post();

            post.setPostContents(faker.lorem().paragraph());
            post.setPostTitle(faker.lorem().sentence(5));
            post.setOwner(proUsersId.get(randomPro));

            postsRepository.save(post);
            for (int j = 0; j < amount; j++) {
                Comment comment = new Comment();
                comment.setCommentContents(faker.lorem().sentence());
                comment.setPost(post);
                commentRepository.save(comment);
            }

        }

    }


    public void generateAd(int amount) {

        List<String> proUsersId = null;
        try {
            proUsersId = tripServiceClient.getAllProUser();
        } catch (Exception e) {
        }
        Random r = new Random();

        Faker faker = new Faker();
        LocalDate now = LocalDate.now();
        for (int i = 0; i < amount; i++) {
            int randomPro = r.nextInt(proUsersId.size());

            Advertisement advertisement = new Advertisement();
            advertisement.setAdvertisementImage(faker.internet().image());
            advertisement.setAdvertisementTitle(faker.lorem().sentence());
            advertisement.setCreatedAt(now);
            advertisement.updatedAt(now);
            advertisement.setExpirationDate(now.plusDays(5));
            advertisement.setOwner(proUsersId.get(randomPro));
            advertisementsRepository.save(advertisement);
        }

    }
}
