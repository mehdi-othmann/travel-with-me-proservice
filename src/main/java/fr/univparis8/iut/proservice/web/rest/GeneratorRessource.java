package fr.univparis8.iut.proservice.web.rest;

import fr.univparis8.iut.proservice.service.GeneratorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GeneratorRessource {

    private final GeneratorService generatorService;

    public GeneratorRessource(GeneratorService generatorService) {
        this.generatorService = generatorService;
    }


    @GetMapping("/generate/post/{amount}")
    public void GenerateCountry(@PathVariable int amount) {
        generatorService.generatePost(amount);
    }

    @GetMapping("/generate/ad/{amount}")
    public void GenerateAd(@PathVariable int amount) {
        generatorService.generateAd(amount);
    }


}
