/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.univparis8.iut.proservice.web.rest.vm;
