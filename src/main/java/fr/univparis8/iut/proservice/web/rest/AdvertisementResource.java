package fr.univparis8.iut.proservice.web.rest;
import fr.univparis8.iut.proservice.service.AdvertisementService;
import fr.univparis8.iut.proservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.proservice.web.rest.util.HeaderUtil;
import fr.univparis8.iut.proservice.service.dto.AdvertisementDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Advertisement.
 */
@RestController
@RequestMapping("/api")
public class AdvertisementResource {

    private final Logger log = LoggerFactory.getLogger(AdvertisementResource.class);

    private static final String ENTITY_NAME = "travelWithMeProServiceAdvertisement";

    private final AdvertisementService advertisementService;

    public AdvertisementResource(AdvertisementService advertisementService) {
        this.advertisementService = advertisementService;
    }

    /**
     * POST  /advertisements : Create a new advertisement.
     *
     * @param advertisementDTO the advertisementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new advertisementDTO, or with status 400 (Bad Request) if the advertisement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/advertisements")
    public ResponseEntity<AdvertisementDTO> createAdvertisement(@Valid @RequestBody AdvertisementDTO advertisementDTO) throws URISyntaxException {
        log.debug("REST request to save Advertisement : {}", advertisementDTO);
        if (advertisementDTO.getId() != null) {
            throw new BadRequestAlertException("A new advertisement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvertisementDTO result = advertisementService.save(advertisementDTO);
        return ResponseEntity.created(new URI("/api/advertisements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /advertisements : Updates an existing advertisement.
     *
     * @param advertisementDTO the advertisementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated advertisementDTO,
     * or with status 400 (Bad Request) if the advertisementDTO is not valid,
     * or with status 500 (Internal Server Error) if the advertisementDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/advertisements")
    public ResponseEntity<AdvertisementDTO> updateAdvertisement(@Valid @RequestBody AdvertisementDTO advertisementDTO) throws URISyntaxException {
        log.debug("REST request to update Advertisement : {}", advertisementDTO);
        if (advertisementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdvertisementDTO result = advertisementService.save(advertisementDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, advertisementDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /advertisements : get all the advertisements.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of advertisements in body
     */
    @GetMapping("/advertisements")
    public List<AdvertisementDTO> getAllAdvertisements() {
        log.debug("REST request to get all Advertisements");
        return advertisementService.findAll();
    }

    /**
     * GET  /advertisements/:id : get the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the advertisementDTO, or with status 404 (Not Found)
     */
    @GetMapping("/advertisements/{id}")
    public ResponseEntity<AdvertisementDTO> getAdvertisement(@PathVariable Long id) {
        log.debug("REST request to get Advertisement : {}", id);
        Optional<AdvertisementDTO> advertisementDTO = advertisementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advertisementDTO);
    }

    /**
     * DELETE  /advertisements/:id : delete the "id" advertisement.
     *
     * @param id the id of the advertisementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/advertisements/{id}")
    public ResponseEntity<Void> deleteAdvertisement(@PathVariable Long id) {
        log.debug("REST request to delete Advertisement : {}", id);
        advertisementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
