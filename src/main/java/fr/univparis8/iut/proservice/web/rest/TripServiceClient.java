package fr.univparis8.iut.proservice.web.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("travelwithmetripservice")
public interface TripServiceClient {

    @RequestMapping(method = RequestMethod.GET, value = "/api/proUsersId")
    List<String> getAllProUser();
}
