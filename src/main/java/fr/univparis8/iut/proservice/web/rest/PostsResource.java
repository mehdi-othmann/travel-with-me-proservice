package fr.univparis8.iut.proservice.web.rest;

import fr.univparis8.iut.proservice.service.PostService;
import fr.univparis8.iut.proservice.service.PostsService;
import fr.univparis8.iut.proservice.service.dto.PostDTO;
import fr.univparis8.iut.proservice.service.dto.PostsDTO;
import fr.univparis8.iut.proservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.proservice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Post.
 */
@RestController
@RequestMapping("/api")
public class PostsResource {

    private final Logger log = LoggerFactory.getLogger(PostsResource.class);

    private static final String ENTITY_NAME = "travelWithMeProServicePost";

    private final PostsService postsService;

    public PostsResource(PostsService postsService) {
        this.postsService = postsService;
    }

    @GetMapping("/posts/user/{login}")
    public List<PostsDTO> getUserTrips(@PathVariable String login) {
        return postsService.findUserPosts(login);
    }

}
