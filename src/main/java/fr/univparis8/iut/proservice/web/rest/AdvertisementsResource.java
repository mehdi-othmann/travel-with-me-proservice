package fr.univparis8.iut.proservice.web.rest;

import fr.univparis8.iut.proservice.service.AdvertisementsService;
import fr.univparis8.iut.proservice.service.dto.AdvertisementsDTO;
import fr.univparis8.iut.proservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.proservice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Advertisement.
 */
@RestController
@RequestMapping("/api")
public class AdvertisementsResource {

    private final Logger log = LoggerFactory.getLogger(AdvertisementsResource.class);

    private static final String ENTITY_NAME = "travelWithMeProServiceAdvertisement";

    private final AdvertisementsService advertisementsService;

    public AdvertisementsResource(AdvertisementsService advertisementsService) {
        this.advertisementsService = advertisementsService;
    }


    @PostMapping("/pub")
    public ResponseEntity<AdvertisementsDTO> createAdvertisement(@Valid @RequestBody AdvertisementsDTO advertisementsDTO) throws URISyntaxException {
        if (advertisementsDTO.getId() != null) {
            throw new BadRequestAlertException("A new advertisement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdvertisementsDTO result = advertisementsService.save(advertisementsDTO);
        return ResponseEntity.created(new URI("/api/advertisements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @GetMapping("/pub")
    public List<AdvertisementsDTO> getAllAdvertisements() {
        return advertisementsService.findAll();
    }

    @GetMapping("/pub/user/{userLogin}")
    public List<AdvertisementsDTO> getProAdvertisements(@PathVariable String userLogin) {
        return advertisementsService.findAllByOwner(userLogin);
    }

    @GetMapping("/pub/{id}")
    public ResponseEntity<AdvertisementsDTO> getAdvertisement(@PathVariable Long id) {
        log.debug("REST request to get Advertisement : {}", id);
        Optional<AdvertisementsDTO> advertisementDTO = advertisementsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advertisementDTO);
    }

    @DeleteMapping("/pub/{id}")
    public ResponseEntity<Void> deleteAdvertisement(@PathVariable Long id) {
        log.debug("REST request to delete Advertisement : {}", id);
        advertisementsService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
