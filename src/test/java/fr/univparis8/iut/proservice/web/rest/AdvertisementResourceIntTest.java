package fr.univparis8.iut.proservice.web.rest;

import fr.univparis8.iut.proservice.TravelWithMeProServiceApp;

import fr.univparis8.iut.proservice.domain.Advertisement;
import fr.univparis8.iut.proservice.repository.AdvertisementRepository;
import fr.univparis8.iut.proservice.service.AdvertisementService;
import fr.univparis8.iut.proservice.service.dto.AdvertisementDTO;
import fr.univparis8.iut.proservice.service.mapper.AdvertisementMapper;
import fr.univparis8.iut.proservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static fr.univparis8.iut.proservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdvertisementResource REST controller.
 *
 * @see AdvertisementResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TravelWithMeProServiceApp.class)
public class AdvertisementResourceIntTest {

    private static final String DEFAULT_ADVERTISEMENT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_ADVERTISEMENT_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_ADVERTISEMENT_IMAGE = "AAAAAAAAAA";
    private static final String UPDATED_ADVERTISEMENT_IMAGE = "BBBBBBBBBB";

    private static final String DEFAULT_OWNER = "AAAAAAAAAA";
    private static final String UPDATED_OWNER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EXPIRATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPIRATION_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Autowired
    private AdvertisementMapper advertisementMapper;

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAdvertisementMockMvc;

    private Advertisement advertisement;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdvertisementResource advertisementResource = new AdvertisementResource(advertisementService);
        this.restAdvertisementMockMvc = MockMvcBuilders.standaloneSetup(advertisementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advertisement createEntity(EntityManager em) {
        Advertisement advertisement = new Advertisement()
            .advertisementTitle(DEFAULT_ADVERTISEMENT_TITLE)
            .advertisementImage(DEFAULT_ADVERTISEMENT_IMAGE)
            .owner(DEFAULT_OWNER)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .expirationDate(DEFAULT_EXPIRATION_DATE);
        return advertisement;
    }

    @Before
    public void initTest() {
        advertisement = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdvertisement() throws Exception {
        int databaseSizeBeforeCreate = advertisementRepository.findAll().size();

        // Create the Advertisement
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);
        restAdvertisementMockMvc.perform(post("/api/advertisements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advertisementDTO)))
            .andExpect(status().isCreated());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeCreate + 1);
        Advertisement testAdvertisement = advertisementList.get(advertisementList.size() - 1);
        assertThat(testAdvertisement.getAdvertisementTitle()).isEqualTo(DEFAULT_ADVERTISEMENT_TITLE);
        assertThat(testAdvertisement.getAdvertisementImage()).isEqualTo(DEFAULT_ADVERTISEMENT_IMAGE);
        assertThat(testAdvertisement.getOwner()).isEqualTo(DEFAULT_OWNER);
        assertThat(testAdvertisement.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAdvertisement.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testAdvertisement.getExpirationDate()).isEqualTo(DEFAULT_EXPIRATION_DATE);
    }

    @Test
    @Transactional
    public void createAdvertisementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = advertisementRepository.findAll().size();

        // Create the Advertisement with an existing ID
        advertisement.setId(1L);
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvertisementMockMvc.perform(post("/api/advertisements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advertisementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkAdvertisementTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setAdvertisementTitle(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc.perform(post("/api/advertisements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advertisementDTO)))
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAdvertisementImageIsRequired() throws Exception {
        int databaseSizeBeforeTest = advertisementRepository.findAll().size();
        // set the field null
        advertisement.setAdvertisementImage(null);

        // Create the Advertisement, which fails.
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        restAdvertisementMockMvc.perform(post("/api/advertisements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advertisementDTO)))
            .andExpect(status().isBadRequest());

        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAdvertisements() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get all the advertisementList
        restAdvertisementMockMvc.perform(get("/api/advertisements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advertisement.getId().intValue())))
            .andExpect(jsonPath("$.[*].advertisementTitle").value(hasItem(DEFAULT_ADVERTISEMENT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].advertisementImage").value(hasItem(DEFAULT_ADVERTISEMENT_IMAGE.toString())))
            .andExpect(jsonPath("$.[*].owner").value(hasItem(DEFAULT_OWNER.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
            .andExpect(jsonPath("$.[*].expirationDate").value(hasItem(DEFAULT_EXPIRATION_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getAdvertisement() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        // Get the advertisement
        restAdvertisementMockMvc.perform(get("/api/advertisements/{id}", advertisement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(advertisement.getId().intValue()))
            .andExpect(jsonPath("$.advertisementTitle").value(DEFAULT_ADVERTISEMENT_TITLE.toString()))
            .andExpect(jsonPath("$.advertisementImage").value(DEFAULT_ADVERTISEMENT_IMAGE.toString()))
            .andExpect(jsonPath("$.owner").value(DEFAULT_OWNER.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.expirationDate").value(DEFAULT_EXPIRATION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAdvertisement() throws Exception {
        // Get the advertisement
        restAdvertisementMockMvc.perform(get("/api/advertisements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdvertisement() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        int databaseSizeBeforeUpdate = advertisementRepository.findAll().size();

        // Update the advertisement
        Advertisement updatedAdvertisement = advertisementRepository.findById(advertisement.getId()).get();
        // Disconnect from session so that the updates on updatedAdvertisement are not directly saved in db
        em.detach(updatedAdvertisement);
        updatedAdvertisement
            .advertisementTitle(UPDATED_ADVERTISEMENT_TITLE)
            .advertisementImage(UPDATED_ADVERTISEMENT_IMAGE)
            .owner(UPDATED_OWNER)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .expirationDate(UPDATED_EXPIRATION_DATE);
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(updatedAdvertisement);

        restAdvertisementMockMvc.perform(put("/api/advertisements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advertisementDTO)))
            .andExpect(status().isOk());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeUpdate);
        Advertisement testAdvertisement = advertisementList.get(advertisementList.size() - 1);
        assertThat(testAdvertisement.getAdvertisementTitle()).isEqualTo(UPDATED_ADVERTISEMENT_TITLE);
        assertThat(testAdvertisement.getAdvertisementImage()).isEqualTo(UPDATED_ADVERTISEMENT_IMAGE);
        assertThat(testAdvertisement.getOwner()).isEqualTo(UPDATED_OWNER);
        assertThat(testAdvertisement.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testAdvertisement.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testAdvertisement.getExpirationDate()).isEqualTo(UPDATED_EXPIRATION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingAdvertisement() throws Exception {
        int databaseSizeBeforeUpdate = advertisementRepository.findAll().size();

        // Create the Advertisement
        AdvertisementDTO advertisementDTO = advertisementMapper.toDto(advertisement);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvertisementMockMvc.perform(put("/api/advertisements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advertisementDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Advertisement in the database
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdvertisement() throws Exception {
        // Initialize the database
        advertisementRepository.saveAndFlush(advertisement);

        int databaseSizeBeforeDelete = advertisementRepository.findAll().size();

        // Delete the advertisement
        restAdvertisementMockMvc.perform(delete("/api/advertisements/{id}", advertisement.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Advertisement> advertisementList = advertisementRepository.findAll();
        assertThat(advertisementList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Advertisement.class);
        Advertisement advertisement1 = new Advertisement();
        advertisement1.setId(1L);
        Advertisement advertisement2 = new Advertisement();
        advertisement2.setId(advertisement1.getId());
        assertThat(advertisement1).isEqualTo(advertisement2);
        advertisement2.setId(2L);
        assertThat(advertisement1).isNotEqualTo(advertisement2);
        advertisement1.setId(null);
        assertThat(advertisement1).isNotEqualTo(advertisement2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdvertisementDTO.class);
        AdvertisementDTO advertisementDTO1 = new AdvertisementDTO();
        advertisementDTO1.setId(1L);
        AdvertisementDTO advertisementDTO2 = new AdvertisementDTO();
        assertThat(advertisementDTO1).isNotEqualTo(advertisementDTO2);
        advertisementDTO2.setId(advertisementDTO1.getId());
        assertThat(advertisementDTO1).isEqualTo(advertisementDTO2);
        advertisementDTO2.setId(2L);
        assertThat(advertisementDTO1).isNotEqualTo(advertisementDTO2);
        advertisementDTO1.setId(null);
        assertThat(advertisementDTO1).isNotEqualTo(advertisementDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(advertisementMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(advertisementMapper.fromId(null)).isNull();
    }
}
